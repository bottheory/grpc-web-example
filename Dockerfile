FROM golang:1.14-alpine as build

WORKDIR /src

ADD . /src/

# RUN apk add --no-cache openssl

# RUN openssl req -x509 -nodes -days 365 -subj "/C=CA/ST=QC/O=Company, Inc./CN=mydomain.com" \
#                -newkey rsa:2048 -keyout ./selfsigned.key -out ./selfsigned.crt

# RUN chmod 444 selfsigned.key
# RUN chmod 444 selfsigned.crt

RUN apk add --no-cache git protobuf && \
    go get github.com/golang/protobuf/protoc-gen-go && \
    protoc --proto_path=proto --go_out=./go/_proto proto/examplecom/library/*.proto

WORKDIR /src/go

RUN GOPATH="/go" GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go install -installsuffix "static" && \
    echo 'nobody:x:65534:' > /src/group.nobody && \
    echo 'nobody:x:65534:65534::/:' > /src/passwd.nobody && \
    GRPC_HEALTH_PROBE_VERSION=v0.2.2 && \
    wget -q -O /bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

FROM gcr.io/distroless/static

ENV PORT=8080
EXPOSE 8080

COPY --from=build /src/group.nobody /etc/group
COPY --from=build /src/passwd.nobody /etc/passwd
USER nobody:nobody

COPY --from=build /go/bin/exampleserver /bin/exampleserver

#COPY --from=build /src/selfsigned.key /bin/selfsigned.key
#COPY --from=build /src/selfsigned.crt /bin/selfsigned.crt

ENTRYPOINT ["/bin/exampleserver"]
