#!/bin/bash

mkdir -p ./ts/_proto
mkdir -p ./go/_proto

if [[ "$GOBIN" == "" ]]; then
  if [[ "$GOPATH" == "" ]]; then
    echo "Required env var GOPATH is not set; aborting with error; see the following documentation which can be invoked via the 'go help gopath' command."
    go help gopath
    exit -1
  fi

  echo "Optional env var GOBIN is not set; using default derived from GOPATH as: \"$GOPATH/bin\""
  export GOBIN="$GOPATH/bin"
fi

PROTOC=`command -v protoc`
if [[ "$PROTOC" == "" ]]; then
  echo "Required "protoc" to be installed. Please visit https://github.com/protocolbuffers/protobuf/releases (3.5.0 suggested)."
  exit -1
fi

# Install protoc-gen-go from the vendored protobuf package to $GOBIN
#(cd ../../vendor/github.com/golang/protobuf && make install)

echo "Compiling protobuf definitions"
	protoc \
    --proto_path=proto \
    --go_out=./go/_proto \
    --go-grpc_out=./go/_proto \
	  --plugin="protoc-gen-ts=./node_modules/ts-protoc-gen/bin/protoc-gen-ts" \
	  --js_out=import_style=commonjs,binary:./ts/_proto --ts_out=service=grpc-web:./ts/_proto \
	  proto/examplecom/library/*.proto
