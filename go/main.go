package main

import (
	"exampleserver/_proto/examplecom/library"
	"flag"
	"fmt"
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

var (
	enableTls       = flag.Bool("enable_tls", false, "Use TLS - required for HTTP2.")
	tlsCertFilePath = flag.String("tls_cert_file", "../../misc/localhost.crt", "Path to the CRT/PEM file.")
	tlsKeyFilePath  = flag.String("tls_key_file", "../../misc/localhost.key", "Path to the private key file.")
)

func main() {
	flag.Parse()

	echan := make(chan error)
	hport := os.Getenv("HC_PORT")
	if len(hport) != 0 {
		go func() {
			http.HandleFunc("/", hello)
			err := http.ListenAndServe(":"+hport, nil)
			echan <- err
		}()
	}

	var port int
	if p, err := strconv.Atoi(os.Getenv("GRPC_PORT")); err != nil {
		log.Printf("Error getting port from env var GRPC_PORT: %v, defaulting to 9090", err)
		port = 9090
	} else {
		port = p
	}
	if *enableTls {
		port = 9091
	}

	mode := os.Getenv("MODE")

	grpcServer := grpc.NewServer()
	if strings.Compare(mode, "book") == 0 {
		library.RegisterBookServiceServer(grpcServer, &bookService{})
		library.RegisterEchoServer(grpcServer, &echoServer{})
	} else {
		library.RegisterEchoServer(grpcServer, &echoServer{})
	}
	grpclog.SetLogger(log.New(os.Stdout, "exampleserver: ", log.LstdFlags))

	wrappedServer := grpcweb.WrapServer(grpcServer)
	handler := func(resp http.ResponseWriter, req *http.Request) {
		setupResponse(&resp, req)
		if (*req).Method == "OPTIONS" {
			return
		}

		wrappedServer.ServeHTTP(resp, req)
	}

	httpServer := http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: http.HandlerFunc(handler),
	}

	grpclog.Printf("Starting server. http port: %d, with TLS: %v", port, *enableTls)

	if *enableTls {
		if err := httpServer.ListenAndServeTLS(*tlsCertFilePath, *tlsKeyFilePath); err != nil {
			grpclog.Fatalf("failed starting http2 server: %v", err)
		}
	} else {
		if err := httpServer.ListenAndServe(); err != nil {
			grpclog.Fatalf("failed starting http server: %v", err)
		}
	}

}

func hello(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "hello\n")
	//log.Printf("hc called")
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, x-grpc-web")
}
