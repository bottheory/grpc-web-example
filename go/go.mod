module exampleserver

go 1.16

require (
	github.com/desertbit/timer v0.0.0-20180107155436-c41aec40b27f // indirect
	github.com/golang/protobuf v1.4.2
	github.com/improbable-eng/grpc-web v0.14.0
	github.com/rs/cors v1.7.0 // indirect
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.25.0
	nhooyr.io/websocket v1.8.7 // indirect
)
