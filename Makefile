protos:
	protoc --proto_path=proto --go_out=./go/_proto --go-grpc_out=./go/_proto \
	--plugin="protoc-gen-ts=./node_modules/ts-protoc-gen/bin/protoc-gen-ts" \
	--js_out=import_style=commonjs,binary:./ts/_proto --ts_out=service=grpc-web:./ts/_proto \
	proto/examplecom/library/*.proto
